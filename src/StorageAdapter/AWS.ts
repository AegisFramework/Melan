import S3 from 'aws-sdk/clients/s3';
import FileStorageAdapter from './../file-storage-adapter';
import Util from './../util';

class AWS extends FileStorageAdapter {
	private _allowedMimeTypes: string[] = [];
	private _endpoint: string = '';
	private _region: string = '';
	private _bucket: string = '';
	private _root: string = '';

	private _instance: S3;

	constructor (configuration: Record<string, any>) {
		super(configuration);

		const { apiVersion, region, endpoint, credentials, allow, bucket, root } = Object.assign({
			region: '',
			endpoint: '',
			allow: [],
			apiVersion: 'latest',
			bucket: '',
			root: '',
		}, configuration);

		const config: Record<string, any> = { apiVersion };

		if (typeof region === 'string' && region !== '') {
			config.region = region;
			this._region = region;
		}

		if (typeof endpoint === 'string' && endpoint !== '') {
			config.endpoint = endpoint;
			this._endpoint = endpoint;
		}

		if (typeof apiVersion === 'string' && apiVersion !== 'latest') {
			config.apiVersion = apiVersion;
		} else {
			config.apiVersion = 'latest';
		}

		if (allow instanceof Array) {
			this._allowedMimeTypes = allow;
		} else {
			this._allowedMimeTypes = [];
		}

		if (typeof credentials === 'object' && credentials !== null) {
			config.credentials = credentials;
		}

		if (typeof root === 'string' && root !== '') {
			this._root = root;
		}

		if (typeof bucket === 'string' && bucket !== '') {
			this._bucket = bucket;
			config.params = { Bucket: bucket };
		}

		this._instance = new S3 (config);
	}

	public instance (): S3 {
		return this._instance;
	}

	public async upload (file: string, path: string): Promise<string>;
	public async upload (file: string, path: string, name: string): Promise<string>;
	public async upload (file: string, path: string, name?: string): Promise<string> {
		return new Promise ((resolve, reject) => {
			if (file.length === 0) {
				throw new Error('No data was provided.');
			}

			const instance = this.instance();
			const indicator = file.substr(0, 20);
			let fileExtension = '';
			let mimeType = '';


			for (const mime of this._allowedMimeTypes) {
				if (indicator.indexOf(mime) > 0) {
					// eslint-disable-next-line prefer-destructuring
					fileExtension = mime.split('/')[1];
					mimeType = mime;
					break;
				}
			}

			if (fileExtension === '') {
				throw new Error('UNSUPPORTED_FORMAT');
			}

			const base64Data = file.replace(`data:${mimeType};base64,`, '');
			const decodedFile = Buffer.from (base64Data, 'base64');

			if (typeof name !== 'string') {
				name = Util.uuid ();
			}

			instance.upload(
				{
					Bucket: this._bucket,
					Body: decodedFile,
					Key: `${this._root}${path}${name}.${fileExtension}`,
					ContentEncoding: 'base64',
					ContentType: mimeType,
				},
				(error: Error, data: any) => {
					if (error) {
						reject (error);
					} else {
						resolve(data.Location);
					}
				}
			);
		});
	}
}

export = AWS;