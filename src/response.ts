const Status: Record<number, string> = {
	100: 'CONTINUE',
	200: 'OK',
	201: 'CREATED',
	202: 'ACCEPTED',
	204: 'NO_CONTENT',
	205: 'RESET_CONTENT',
	301: 'MOVED_PERMANENTLY',
	302: 'FOUND',
	400: 'BAD_REQUEST',
	401: 'UNAUTHORIZED',
	402: 'PAYMENT_REQUIRED',
	403: 'FORBIDDEN',
	404: 'NOT_FOUND',
	405: 'METHOD_NOT_ALLOWED',
	406: 'NOT_ACCEPTABLE',
	409: 'CONFLICT',
	500: 'SERVER_ERROR',
	503: 'SERVICE_UNAVAILABLE',
};

interface ErrorInterface {
	code: number;
	type?: string;
	message?: string;
}

class Response {
	private _status: string = 'OK';
	private _code: number = 200;
	private _type: string = '';
	private _body: Record<string, string | number | null | object> | Array<any>;
	private _headers: Record<string, string | number>;
	private _callback: Function;
	private _context: any

	constructor (context: any, callback: Function) {
		this._context = context;
		this._body = {};
		this._headers = {};
		this._callback = callback;
	}

	public status (status: string): void {
		this._status = status;
	}

	public code (code: number): void {
		this._code = code;
	}

	public body (data: Record<string, string | number | null | object> | Array<any>): void {
		if (data instanceof Array || typeof data === 'string' || typeof data === 'number') {
			this._body = data;
		} else if (typeof data === 'object') {
			this._body = {...data, ...this._body};
		}
	}

	public headers (headers?: Record<string, string | number>): Record<string, string | number> {
		if (typeof headers !== 'undefined') {
			this._headers = Object.assign (this._headers, headers);
		}
		return this._headers;
	}

	public header (key: string, value: string): void {
		this._headers[key] = value;
	}

	public send (): any {
		const data = this.toJSON ();
		return {
			statusCode: this._code,
			headers: this._headers,
			body: typeof data === 'object' ? JSON.stringify(data) : data,
		};
	}

	public error (error: ErrorInterface): any {
		this.code (error.code);
		this.status (Status[error.code]);
		this._type = error.type || '';
		this.body ({
			message: error.message || '',
			type: this._type
		});

		const data = this.toJSON ();

		return {
			code: this._code,
			status: this._status,
			type: this._type || 'SERVER_ERROR',
			...data,
		};
	}

	public allow (origin: string): void {
		if (origin === '*') {
			this.header ('Access-Control-Allow-Origin', origin);
		} else {
			this.headers ({
				'Access-Control-Allow-Origin': origin,
				'Vary': 'Origin',
			});
		}
	}

	public toJSON (): Record<string, string | number | null | object> | Array<any> {
		return this._body;
	}
}

export = Response;