/* eslint-disable @typescript-eslint/no-empty-function */
class FileStorageAdapter {

	constructor (configuration: Record<string, any>) {}

	public async upload (file: string, path: string): Promise<string>;
	public async upload (file: string, path: string, name: string): Promise<string>;
	public async upload (file: string, path: string, name?: string): Promise<string> {
		return '';
	}
}

export = FileStorageAdapter;