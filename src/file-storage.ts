import FileStorageAdapter from './file-storage-adapter';

class FileStorage<T extends FileStorageAdapter> {
	private _adapter: T;
	private _configuration: Record<string, any>;


	constructor (adapter: new(configuration: Record<string, any>) => T, configuration: Record<string, any>) {
		this._configuration = Object.assign ({ root: '/' }, configuration);
		this._adapter = new adapter (this._configuration);
	}

	public async upload (file: string, path: string): Promise<string>;
	public async upload (file: string, path: string, name: string): Promise<string>;
	public async upload (file: string, path: string, name?: string): Promise<string> {
		if (typeof name === 'string') {
			return this._adapter.upload (file, path, name);
		}
		return this._adapter.upload (file, path);
	}
}

export = FileStorage;