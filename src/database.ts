import fs = require('fs');
import { Sequelize, Model, DataTypes } from 'sequelize';

import Secrets from './secrets';

type DatabaseEngine = 'mysql' | 'postgres' | 'sqlite' | 'mariadb' | 'mssql' | undefined;

interface DatabaseInterface {
	models: Record<string, Model>;
	sequelize: any;
	DataTypes: any;
	Sequelize: any;
}

class Database {
	private static _engine: DatabaseEngine = 'mariadb';
	private static _instance: DatabaseInterface;

	public static setup (engine: DatabaseEngine): void {
		this._engine = engine;
	}

	public static async preload (models: Record<string, string> = {}): Promise<DatabaseInterface> {
		const instance = await this.instance ();

		for (const key in models) {
			const model = require(models[key])(instance.sequelize, DataTypes);
			instance.models[key] = model;
		}

		return instance;
	}

	public static async instance (): Promise<DatabaseInterface> {
		if (typeof this._instance === 'undefined') {
			this._instance = {
				models: {},
				sequelize: null,
				DataTypes: null,
				Sequelize: null
			};
			const [database, user, password, host] = await Secrets.decrypt ('DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_HOST');

			if (typeof database !== 'string' || typeof user !== 'string' || typeof password !== 'string' || typeof host !== 'string') {
				throw new Error('Invalid database credentials.');
			}

			let sequelize;

			if (process.env.ENVIRONMENT === 'production' || process.env.ENVIRONMENT === 'staging' || process.env.ENVIRONMENT === 'development') {
				const root = fs.readFileSync(`${__dirname}/../assets/aws-rds.pem`);

				sequelize = new Sequelize(database, user, password, {
					host,
					dialect: this._engine,
					ssl: true,
					dialectOptions: {
						ssl: {
							rejectUnauthorized: true,
							ca: [root],
						},
					},
				});
			} else {
				sequelize = new Sequelize(database, user, password, {
					host,
					dialect: this._engine
				});
			}

			this._instance.sequelize = sequelize;
			this._instance.Sequelize = Sequelize;
			this._instance.DataTypes = DataTypes;
			return this._instance;
		} else {
			return this._instance;
		}
	}

	public static async models (models?: Record<string, string>): Promise<Record<string, Model>> {
		const instance = await this.instance();
		if (typeof models === 'object' && models !== null) {
			for (const key in models) {
				if (!(key in instance.models)) {
					const model = require(models[key])(instance.sequelize, DataTypes);
					instance.models[key] = model;
				}
			}
		}
		return this._instance.models;
	}

	public static async model (name: string, path: string): Promise<Model> {
		const instance = await this.instance();
		if (!(name in instance.models)) {
			const model = require(path)(instance.sequelize, DataTypes);
			instance.models[model.name] = model;
		}
		return this._instance.models[name];
	}
}

export = Database;