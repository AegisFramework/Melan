class Request {

	private _method: string;
	private _domain: string;
	private _resource: string;
	private _headers: Record<string, string>;
	private _queryParameters: Record<string, string | number>;
	private _pathParameters: Record<string, string | number>;
	private _data: Record<string, string | number | object | null>;

	constructor (method: string, domain: string, resource: string, headers: Record<string, string>, data: Record<string, string | number | object | null>, queryParameters: Record<string, string | number>, pathParameters: Record<string, string | number>) {
		this._method = method;
		this._domain = domain;
		this._resource = resource;
		this._headers = headers;

		if (method === 'GET') {
			this._data = queryParameters;
		} else {
			if (typeof data === 'string') {
				this._data = JSON.parse(data);
			} else {
				this._data = data;
			}
		}

		this._queryParameters = queryParameters;
		this._pathParameters = pathParameters;
	}

	public body (keys: string[] | null = null, allowEmpty: boolean = false, allowNull: boolean = false): Record<string, string | number | object | null> {
		if (keys !== null) {
			const object: Record<string, string | number | object | null> = {};
			for (const key of keys) {
				if (typeof this._data[key] !== 'undefined') {
					const value = this._data[key];

					if (!allowEmpty) {
						if (typeof value === 'string') {
							if (value.trim () === '') {
								throw new Error (`Requested key ${key} was empty.`);
							}
						}
					}

					if (!allowNull && value === null) {
						throw new Error (`Requested key ${key} can not be null.`);
					}

					object[key] = value;
				} else {
					if (!allowNull) {
						throw new Error (`Requested key ${key} was not present in the request data.`);
					}
				}
			}

		}
		return this._data;
	}

	get method (): string {
		return this._method;
	}

	get domain (): string {
		return this._domain;
	}

	get resource (): string {
		return this._resource;
	}

	get path (): Record<string, string | number> {
		return this._pathParameters;
	}

	get query (): Record<string, string | number> {
		return this._queryParameters;
	}

	public headers (): Record<string, string> {
		return this._headers;
	}

	public header (key: string): string {
		return this._headers[key];
	}

	public mobile (platform = 'Any'): boolean {
		const userAgent = this.header ('User-Agent');

		let match = false;
		switch (platform) {
			case 'Android':
				match = /Android/i.test (userAgent);
				break;

			case 'iOS':
				match = /iPhone|iPad|iPod/i.test (userAgent);
				break;

			case 'Windows':
				match = /Windows Phone|IEMobile|WPDesktop/i.test (userAgent);
				break;

			case 'BlackBerry':
				match = /BlackBerry|BB10/i.test (userAgent);
				break;

			case 'Any':
			default:
				match = /Android|iPhone|iPad|iPod|Windows Phone|IEMobile|WPDesktop|BlackBerry|BB10/i.test (userAgent);
				break;
		}

		return match;
	}

	public desktop (platform = 'Any'): boolean {
		const userAgent = this.header ('User-Agent');

		let match = false;
		switch (platform) {
			case 'Windows':
				match = userAgent.includes ('Win');
				break;

			case 'macOS':
				match = userAgent.includes ('Mac');
				break;

			case 'Linux':
				match = userAgent.includes ('Linux');
				break;

			case 'FreeBSD':
				match = userAgent.includes ('FreeBSD');
				break;

			case 'webOS':
				match = userAgent.includes ('WebTV');
				break;

			case 'Any':
			default:
				match = userAgent.includes ('Win')
						|| userAgent.includes ('Mac')
						|| userAgent.includes ('Linux')
						|| userAgent.includes ('FreeBSD')
						|| userAgent.includes ('WebTV');
				break;
		}
		return match;
	}

	public secure () {
		return this.header ('X-Forwarded-Proto') === 'https';
	}
}

export = Request;