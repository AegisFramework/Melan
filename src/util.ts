import { v4 as uuidv4 } from 'uuid';

class Util {

	/**
	 * @static callAsync - Calls any function using promises to keep a standard
	 * behavior between async and sync functions.
	 *
	 * @param  {funcion} callable - The function to run
	 * @param  {Object} context - The object `this` will be mapped to
	 * @param  {any} ...args - List of parameters to pass to the function when called
	 * @return {Promise} - A promise that resolves to the result of the function
	 */
	public static callAsync (callable: Function, context: any, ...args: any[]): any {
		try {
			// Call the provided function using the context and arguments given
			const result = callable.apply (context, args);

			// Check if the function returned a simple value or a Promise
			if (result instanceof Promise) {
				return result;
			} else {
				return Promise.resolve (result);
			}
		} catch (e) {
			return Promise.reject (e);
		}
	}

	public static uuid (): string {
		return uuidv4();
	}
}

export = Util;