/**
 * ==============================
 * Router
 * ==============================
 */

import Response from './response';
import Request from './request';
import Secrets from './secrets';
import Token from './token';

interface APIGatewayEvent {
	event? : APIGatewayEvent;
	resource: string;
	path?: string;
	httpMethod?: string;
	headers: Record<string, any>;
	multiValueHeaders?: Record<string, any[]>;
	queryStringParameters: Record<string, any>;
	multiValueQueryStringParameters?: Record<string, any[]>;
    pathParameters: Record<string, any>;
    stageVariables: Record<string, any>;
    requestContext: Record<string, any>;
    body: Record<string, any>;
	isBase64Encoded: boolean;
	http?: {
        method: string;
        path: string;
	};
	source?: string;
}

interface PaginatedResponse {
	previous: string;
	current: string;
	next: string;
	count: number;
	results: any;
}

interface PaginationOptions {
	results: any;
	page: number;
	size: number;
	count: number;
	params: Record<string, string | number>;
}

class Router {

	private _event: APIGatewayEvent;

	private _context: any;
	private _callback: Function
	private _response: Response;
	private _request: Request;
	private _stage: any;
	private _origins: string[] = [];

	public static serialize (params: Record<string, string | number>): string {
		return Object.keys (params)
			.map (key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
			.join ('&');
	}

	public paginate (options: PaginationOptions): PaginatedResponse {
		const { results, size, count, params, page } = options;
		const url = `https://${this._request.domain}${this._request.resource}`;

		return {
			previous: page > 1 ? `${url}?${Router.serialize({ page: page - 1, ...params })}` : '',
			current: `${url}?${Router.serialize({ page, ...params })}`,
			next: page * size < count ? `${url}?${Router.serialize({ page: page + 1, ...params })}` : '',
			count,
			results,
		};
	}

	public static mime (extension: string): string {
		// Text
		if (extension === '.html') {
			return 'text/html';
		} else if (extension === '.php') {
			return 'text/html';
		} else if (extension === '.css') {
			return 'text/css';
		} else if (extension === '.js') {
			return 'text/javascript';
		}

		// Images
		if (extension === '.png') {
			return 'image/png';
		} else if (extension === '.jpg') {
			return 'image/jpg';
		} else if (extension === '.jpeg') {
			return 'image/jpeg';
		} else if (extension === '.gif') {
			return 'image/gif';
		} else if (extension === '.bmp') {
			return 'image/bmp';
		} else if (extension === '.webp') {
			return 'image/webp';
		} else if (extension === '.ico') {
			return 'image/x-icon';
		} else if (extension === '.svg') {
			return 'image/svg+xml';
		}

		// Videos
		if (extension === '.mov') {
			return 'video/quicktime';
		} else if (extension === '.mp4') {
			return 'video/mp4';
		}
		// Audio
		if (extension === '.mp3') {
			return 'audio/mpeg3';
		} else if (extension === '.ogg') {
			return 'audio/ogg';
		} else if (extension === '.flac') {
			return 'audio/flac';
		}

		// Fonts
		if (extension === '.ttf') {
			return 'font/truetype';
		} else if (extension === '.otf') {
			return 'font/opentype';
		} else if (extension === '.woff') {
			return 'application/font-woff';
		}

		// Archives
		if (extension === '.zip') {
			return 'application/octet-stream';
		} else if (extension === '.rar') {
			return 'application/octet-stream';
		} else if (extension === '.tar') {
			return 'application/octet-stream';
		} else if (extension === '.gz') {
			return 'application/octet-stream';
		}

		// Files
		if (extension === '.pdf') {
			return 'application/pdf';
		}

		return 'text/plain';
	}

	constructor (event: APIGatewayEvent, context: Record<string, any>, callback: Function) {
		context.callbackWaitsForEmptyEventLoop = false;
		this._event = event;

		if (typeof this._event.event === 'object') {
			this._event = this._event.event;
		}

		if (typeof this._event.headers !== 'object' || this._event.headers === null) {
			this._event.headers = {};
		}

		this._context = this._event.requestContext;
		this._callback = callback;
		this._stage = this._event.stageVariables || {};

		let method = '';

		if (typeof this._event.httpMethod === 'string') {
			method = this._event.httpMethod;
		} else if (typeof this._event.http === 'object') {
			method = this._event.http.method;
		}

		let path = '';

		if (typeof this._context === 'object' && typeof this._context.path === 'string') {
			path = this._context.path;
		} else if (typeof this._event.http === 'object') {
			path = this._event.http.path;
		}

		this._request = new Request (method, this._context.domainName, path, this._event.headers || {}, this._event.body || {}, this._event.queryStringParameters || {}, this._event.pathParameters || {});
		this._response = new Response (context, callback);
	}

	get request (): Request {
		return this._request;
	}

	get response (): Response {
		return this._response;
	}

	get context (): any {
		return this._context;
	}

	get event (): APIGatewayEvent {
		return this._event;
	}

	get stage (): any {
		return this._stage;
	}

	public allow (...origins: string[]): void {
		this._origins = this._origins.concat(origins);
	}

	public async authorize (ttl: number, sessionKey: string = '', secretKey: string = 'SECRET_KEY'): Promise<{ token: string; payload: Record<string, any> }> {
		let secret;
		let token = '';
		let payload: Record<string, any> = {};
		let accessToken = '';

		const authorization = this.request.header('Authorization');

		if (!authorization) {
			throw {
				code: 401,
				type: 'MISSING_AUTHORIZATION',
				message: 'Missing authorization header',
			};
		}

		const bearerString = authorization.split(' ');

		if (bearerString.length === 2) {
			accessToken = bearerString[1];
		} else if (bearerString.length === 1) {
			accessToken = authorization;
		} else {
			throw {
				code: 403,
				type: 'INVALID_AUTHORIZATION',
				message: 'This access token is not properly formatted.',
			};
		}

		try {
			[secret] = await Secrets.decrypt (secretKey);

			if (typeof secret === 'string') {
				Token.key(secret);
			}
		} catch (error) {
			throw {
				code: 500,
				message: error.message,
			};
		}

		try {
			const data = Token.read(accessToken, ttl);
			token = data.token;
			payload = data.payload;
		} catch (error) {
			throw {
				code: 403,
				type: 'INVALID_AUTHORIZATION',
				message: error.message,
			};
		}

		if (typeof sessionKey === 'string' && sessionKey !== '') {
			if (payload.sessionKey !== sessionKey) {
				throw {
					code: 403,
					type: 'TOKEN_INVALIDATED',
					message: 'This access token has been invalidated, please log in again.',
				};
			}
		}

		return { token, payload };
	}

	public async serve (callback: Function): Promise<void> {
		try {
			const { origin } = this._event.headers;
			if (typeof origin === 'string') {
				if (this._origins.indexOf (origin) === -1 && this._origins.indexOf ('*') === -1) {
					throw {
						code: 400,
						message: `The origin "${origin}" is not allowed to perform actions over this API.`,
					};
				} else {
					const data = await callback.call(this.response);

					if (this._origins.indexOf ('*') > -1) {
						this.response.allow ('*');
					} else {
						this.response.allow (origin);
					}
					this.response.body (data);
					return this.response.send ();
				}
			} else {
				if (this._origins.indexOf ('*') === -1) {
					throw {
						code: 400,
						message: 'Origin not allowed to perform actions over this API.',
					};
				}

				const data = await callback.call(this.response);
				this.response.body (data);
				return this.response.send ();
			}
		} catch (error) {
			const { origin } = this._event.headers;
			if (typeof origin === 'string') {
				if (this._origins.indexOf ('*') > -1) {
					this.response.allow ('*');
				} else if (this._origins.indexOf (origin) > -1) {
					this.response.allow (origin);
				} else {
					this.response.body ({
						code: 400,
						message: 'Origin not allowed to perform actions over this API.',
					});
					return this.response.send ();
				}
			} else {
				if (this._origins.indexOf ('*') === -1) {
					this.response.body ({
						code: 400,
						message: 'Origin not allowed to perform actions over this API.',
					});
					return this.response.send ();
				}
			}

			if (error.message && error.code) {
				this.response.body (this.response.error(error));
				return this.response.send ();
			} else {
				this.response.body (this.response.error({
					code: 500,
					message: error.message,
				}));
				return this.response.send ();
			}
		}
	}
}

export = Router;