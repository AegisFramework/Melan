import KMS, { DecryptRequest } from 'aws-sdk/clients/kms';
import fs = require('fs');

interface SecretsInstance {
	env: string;
	kms?: KMS;
	secrets: Record<string, { [key: string]: { ciphertext: string; arn: string; } } | string | undefined>;
	plain: Record<string, { cipher: string; text: string; }>;
}

const ENVIRONMENT = {
	PRODUCTION: 'production',
	STAGING: 'staging',
	DEVELOPMENT: 'development',
};

class Secrets {
	private static _instance: SecretsInstance;
	private static _file: string = '';

	public static instance (): SecretsInstance {
		if (typeof this._instance === 'undefined') {
			this._instance = { env: ENVIRONMENT.DEVELOPMENT, secrets: {}, plain: {} };

			if (typeof this._file === 'string' && this._file !== '') {
				const contents = fs.readFileSync (this._file, {
					encoding: 'utf-8',
					flag: 'r'
				});

				const parsed = JSON.parse (contents);
				this._instance.secrets = parsed;
			}

			if (process.env.ENVIRONMENT === ENVIRONMENT.PRODUCTION) {
				const kmsInstance = new KMS ();
				this._instance.env = ENVIRONMENT.PRODUCTION;
				this._instance.kms = kmsInstance;
			} else if (process.env.ENVIRONMENT === ENVIRONMENT.STAGING) {
				const kmsInstance = new KMS ();
				this._instance.env = ENVIRONMENT.STAGING;
				this._instance.kms = kmsInstance;
			} else if (process.env.ENVIRONMENT === ENVIRONMENT.DEVELOPMENT) {
				const kmsInstance = new KMS ();
				this._instance.env = ENVIRONMENT.DEVELOPMENT;
				this._instance.kms = kmsInstance;
			}

			this._instance.secrets = { ...this._instance.secrets ,...process.env };
		}
		return this._instance;
	}

	static file (path: string): void {
		this._file = path;
	}

	static environment (): string {
		return this.instance().env;
	}

	static async decrypt (...keys: string[]): Promise<Array<string|{ [key: string]: { ciphertext: string; arn: string; } } >> {
		const data = [];
		const env = this.environment();
		if (env === ENVIRONMENT.PRODUCTION || env === ENVIRONMENT.STAGING || env === ENVIRONMENT.DEVELOPMENT) {
			const { kms, secrets, plain } = this.instance();

			if (typeof kms !== 'object') {
				throw new Error ('The AWS KMS tool has not been initialized.');
			}

			for (const key of keys) {
				const request: DecryptRequest = { CiphertextBlob: '' };
				const raw = secrets[key];

				if (typeof raw === 'string') {
					if (key in plain) {
						const { cipher, text } = plain[key];
						if (cipher === raw) {
							data.push(text);
							continue;
						}
					} else {
						plain[key] = {
							cipher: '',
							text: '',
						};
					}

					request.CiphertextBlob = Buffer.from (raw, 'base64');
					if (process.env.AWS_LAMBDA_FUNCTION_NAME) {
						request.EncryptionContext = { LambdaFunctionName: process.env.AWS_LAMBDA_FUNCTION_NAME };
					}
					plain[key].cipher = raw;
				} else if (typeof raw === 'object') {
					const { ciphertext, arn } = raw;

					if (typeof ciphertext === 'string' && typeof arn === 'string') {
						if (key in plain) {
							const { cipher, text } = plain[key];
							if (cipher === ciphertext) {
								data.push(text);
								continue;
							}
						} else {
							plain[key] = {
								cipher: '',
								text: '',
							};
						}

						request.CiphertextBlob = Buffer.from (ciphertext, 'base64');
						request.EncryptionContext = { PARAMETER_ARN: arn };
						plain[key].cipher = ciphertext;
					}
				} else {
					throw new Error (`The environment variable "${key}" is not defined.`);
				}

				const decrypted = await kms.decrypt (request).promise();

				const plainText = decrypted.Plaintext;

				if (typeof plainText === 'undefined') {
					throw new Error(`The environment key "${key}" yielded an undefined value.`);
				}

				plain[key].text = plainText.toString ('ascii');

				data.push(plain[key].text);
			}
		} else {
			const { secrets } = this.instance ();

			if (typeof secrets !== 'object') {
				throw new Error ('The AWS KMS tool has not been initialized.');
			}

			for (const key of keys) {
				const raw = secrets[key];

				if (typeof raw === 'undefined') {
					throw new Error (`The environment variable "${key}" is not defined.`);
				}

				data.push(raw);
			}
		}

		return data;
	}

	static async raw (...keys: string[]): Promise<Array<string|{ [key: string]: { ciphertext: string; arn: string; } } >> {
		const data: any[] = [];
		const { secrets } = this.instance ();

		if (typeof secrets !== 'object') {
			throw new Error ('The AWS KMS tool has not been initialized.');
		}

		for (const key of keys) {
			const raw = secrets[key];

			if (typeof raw === 'undefined') {
				throw new Error (`The environment variable "${key}" is not defined.`);
			}

			data.push(raw);
		}

		return data;
	}
}

export = Secrets;