import branca = require ('branca');

interface Branca {
	encode: Function;
	decode: Function;
}

interface TokenInstance {
	token: string;
	payload: Record<string, any>;
}

class Token {
	private static _key: string;
	private static _instance: Branca;

	public static instance (): Branca {
		if (typeof Token._instance === 'undefined') {
			Token._instance = branca(Token._key);
		}
		return Token._instance;
	}

	public static key (key: string): void {
		Token._key = key;
	}

	public static read (token: string): TokenInstance;
	public static read (token: string, ttl: number): TokenInstance;
	public static read (token: string, ttl?: number): TokenInstance {
		let content = null;

		if (typeof ttl !== 'number') {
			content = Token.instance().decode(token);
		} else {
			content = Token.instance().decode(token, ttl);
		}

		const payload = JSON.parse(content);

		return { token, payload };
	}

	public static create (payload: Record<string, any>): TokenInstance;
	public static create (payload: Record<string, any>, ttl: number): TokenInstance;
	public static create (payload: Record<string, any>, ttl?: number): TokenInstance {

		if (typeof ttl === 'number') {
			return {
				token: Token.instance().encode(JSON.stringify(payload), ttl),
				payload,
			};
		}

		return {
			token: Token.instance().encode(JSON.stringify(payload)),
			payload,
		};
	}

	public static payload (token: string, payload?: Record<string, any>): TokenInstance {
		if (typeof payload !== 'undefined') {
			const previousPayload = Token.read(token).payload;
			return Token.create({
				...previousPayload,
				...payload
			});
		}
		return Token.read(token);
	}
}

export = Token;