import bcrypt = require ('bcrypt');

class Password {
	public static rounds: number = 12;

	public static async hash (password: string): Promise<string> {
		return bcrypt.hash(password, Password.rounds);
	}

	public static async compare (password: string, hash: string): Promise<boolean> {
		return bcrypt.compare(password, hash);
	}
}

export = Password;