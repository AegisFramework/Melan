# Melan

Melan is a minified version of the Aegis Framework designed to work in serverless functions. It provides simple utilities to handle user authorization and authentication as well as managing secrets provided through the process environment encrypted with KMS.

# Usage
Melan is provided as a NodeJS library.

```javascript
const { Password, Secrets, Token } = require ('@aegis-framework/melan');

// If you want to avoid any overhead of requiring all the modules when you only need one
const Password = require ('@aegis-framework/melan/lib/password');
const Secrets = require ('@aegis-framework/melan/lib/secrets');
const Token = require ('@aegis-framework/melan/lib/token');
```

## Password

```javascript
const Password = require ('@aegis-framework/melan/lib/password');

const hash = await Password.hash('somePassword');

const match = await Password.compare('somePassword', hash);
```

## Secrets

```javascript
const Secrets = require ('@aegis-framework/melan/lib/secrets');

// Define a config.json for non-production environment
Secrets.config(__basename + '/config.json');

const [user, password] = await Secrets.decrypt('DB_USER', 'DB_PASSWORD');
```

## Token
The token class provides a simple way to create encrypted tokens


```javascript
const Token = require ('@aegis-framework/melan/lib/token');

// Set the encryption key
Token.key('SetHereA32Bit/CharacterSecretKey');

// Create a token. The create function will return both the token and the payload
// you used to create it.
let { token, payload } = Token.create({
	user: 'example@example.com'
	scopes: ['read', 'write', 'delete']
});

// The read function accepts a token and a ttl value in seconds. If token provided
// is not older than the ttl, it will be updated and returned by the function next
// to the payload it contained. If the token has already expired, this will throw
// an exception.
let { token, payload } = Token.read(token, 3600);
```

# Ready for API Gateway

```javascript
exports.handler = async (event, context, callback) => {
	const router = new Router(event, context, callback);
  	const { request, response } = router;

	router.allow('*');

	return router.serve(async function () {
		// Make sure the request comes with a valid JWT token
		const { token, payload } = await router.authorize(1296000);

		return { success: true };
	});
};
```

# License
Melan is a Open Source Software project released under the MIT License.