"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const response_1 = __importDefault(require("./response"));
const request_1 = __importDefault(require("./request"));
const secrets_1 = __importDefault(require("./secrets"));
const token_1 = __importDefault(require("./token"));
class Router {
    constructor(event, context, callback) {
        this._origins = [];
        context.callbackWaitsForEmptyEventLoop = false;
        this._event = event;
        if (typeof this._event.event === 'object') {
            this._event = this._event.event;
        }
        if (typeof this._event.headers !== 'object' || this._event.headers === null) {
            this._event.headers = {};
        }
        this._context = this._event.requestContext;
        this._callback = callback;
        this._stage = this._event.stageVariables || {};
        let method = '';
        if (typeof this._event.httpMethod === 'string') {
            method = this._event.httpMethod;
        }
        else if (typeof this._event.http === 'object') {
            method = this._event.http.method;
        }
        let path = '';
        if (typeof this._context === 'object' && typeof this._context.path === 'string') {
            path = this._context.path;
        }
        else if (typeof this._event.http === 'object') {
            path = this._event.http.path;
        }
        this._request = new request_1.default(method, this._context.domainName, path, this._event.headers || {}, this._event.body || {}, this._event.queryStringParameters || {}, this._event.pathParameters || {});
        this._response = new response_1.default(context, callback);
    }
    static serialize(params) {
        return Object.keys(params)
            .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
            .join('&');
    }
    paginate(options) {
        const { results, size, count, params, page } = options;
        const url = `https://${this._request.domain}${this._request.resource}`;
        return {
            previous: page > 1 ? `${url}?${Router.serialize({ page: page - 1, ...params })}` : '',
            current: `${url}?${Router.serialize({ page, ...params })}`,
            next: page * size < count ? `${url}?${Router.serialize({ page: page + 1, ...params })}` : '',
            count,
            results,
        };
    }
    static mime(extension) {
        if (extension === '.html') {
            return 'text/html';
        }
        else if (extension === '.php') {
            return 'text/html';
        }
        else if (extension === '.css') {
            return 'text/css';
        }
        else if (extension === '.js') {
            return 'text/javascript';
        }
        if (extension === '.png') {
            return 'image/png';
        }
        else if (extension === '.jpg') {
            return 'image/jpg';
        }
        else if (extension === '.jpeg') {
            return 'image/jpeg';
        }
        else if (extension === '.gif') {
            return 'image/gif';
        }
        else if (extension === '.bmp') {
            return 'image/bmp';
        }
        else if (extension === '.webp') {
            return 'image/webp';
        }
        else if (extension === '.ico') {
            return 'image/x-icon';
        }
        else if (extension === '.svg') {
            return 'image/svg+xml';
        }
        if (extension === '.mov') {
            return 'video/quicktime';
        }
        else if (extension === '.mp4') {
            return 'video/mp4';
        }
        if (extension === '.mp3') {
            return 'audio/mpeg3';
        }
        else if (extension === '.ogg') {
            return 'audio/ogg';
        }
        else if (extension === '.flac') {
            return 'audio/flac';
        }
        if (extension === '.ttf') {
            return 'font/truetype';
        }
        else if (extension === '.otf') {
            return 'font/opentype';
        }
        else if (extension === '.woff') {
            return 'application/font-woff';
        }
        if (extension === '.zip') {
            return 'application/octet-stream';
        }
        else if (extension === '.rar') {
            return 'application/octet-stream';
        }
        else if (extension === '.tar') {
            return 'application/octet-stream';
        }
        else if (extension === '.gz') {
            return 'application/octet-stream';
        }
        if (extension === '.pdf') {
            return 'application/pdf';
        }
        return 'text/plain';
    }
    get request() {
        return this._request;
    }
    get response() {
        return this._response;
    }
    get context() {
        return this._context;
    }
    get event() {
        return this._event;
    }
    get stage() {
        return this._stage;
    }
    allow(...origins) {
        this._origins = this._origins.concat(origins);
    }
    async authorize(ttl, sessionKey = '', secretKey = 'SECRET_KEY') {
        let secret;
        let token = '';
        let payload = {};
        let accessToken = '';
        const authorization = this.request.header('Authorization');
        if (!authorization) {
            throw {
                code: 401,
                type: 'MISSING_AUTHORIZATION',
                message: 'Missing authorization header',
            };
        }
        const bearerString = authorization.split(' ');
        if (bearerString.length === 2) {
            accessToken = bearerString[1];
        }
        else if (bearerString.length === 1) {
            accessToken = authorization;
        }
        else {
            throw {
                code: 403,
                type: 'INVALID_AUTHORIZATION',
                message: 'This access token is not properly formatted.',
            };
        }
        try {
            [secret] = await secrets_1.default.decrypt(secretKey);
            if (typeof secret === 'string') {
                token_1.default.key(secret);
            }
        }
        catch (error) {
            throw {
                code: 500,
                message: error.message,
            };
        }
        try {
            const data = token_1.default.read(accessToken, ttl);
            token = data.token;
            payload = data.payload;
        }
        catch (error) {
            throw {
                code: 403,
                type: 'INVALID_AUTHORIZATION',
                message: error.message,
            };
        }
        if (typeof sessionKey === 'string' && sessionKey !== '') {
            if (payload.sessionKey !== sessionKey) {
                throw {
                    code: 403,
                    type: 'TOKEN_INVALIDATED',
                    message: 'This access token has been invalidated, please log in again.',
                };
            }
        }
        return { token, payload };
    }
    async serve(callback) {
        try {
            const { origin } = this._event.headers;
            if (typeof origin === 'string') {
                if (this._origins.indexOf(origin) === -1 && this._origins.indexOf('*') === -1) {
                    throw {
                        code: 400,
                        message: `The origin "${origin}" is not allowed to perform actions over this API.`,
                    };
                }
                else {
                    const data = await callback.call(this.response);
                    if (this._origins.indexOf('*') > -1) {
                        this.response.allow('*');
                    }
                    else {
                        this.response.allow(origin);
                    }
                    this.response.body(data);
                    return this.response.send();
                }
            }
            else {
                if (this._origins.indexOf('*') === -1) {
                    throw {
                        code: 400,
                        message: 'Origin not allowed to perform actions over this API.',
                    };
                }
                const data = await callback.call(this.response);
                this.response.body(data);
                return this.response.send();
            }
        }
        catch (error) {
            const { origin } = this._event.headers;
            if (typeof origin === 'string') {
                if (this._origins.indexOf('*') > -1) {
                    this.response.allow('*');
                }
                else if (this._origins.indexOf(origin) > -1) {
                    this.response.allow(origin);
                }
                else {
                    this.response.body({
                        code: 400,
                        message: 'Origin not allowed to perform actions over this API.',
                    });
                    return this.response.send();
                }
            }
            else {
                if (this._origins.indexOf('*') === -1) {
                    this.response.body({
                        code: 400,
                        message: 'Origin not allowed to perform actions over this API.',
                    });
                    return this.response.send();
                }
            }
            if (error.message && error.code) {
                this.response.body(this.response.error(error));
                return this.response.send();
            }
            else {
                this.response.body(this.response.error({
                    code: 500,
                    message: error.message,
                }));
                return this.response.send();
            }
        }
    }
}
module.exports = Router;
