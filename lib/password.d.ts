declare class Password {
    static rounds: number;
    static hash(password: string): Promise<string>;
    static compare(password: string, hash: string): Promise<boolean>;
}
export = Password;
