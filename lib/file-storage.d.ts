import FileStorageAdapter from './file-storage-adapter';
declare class FileStorage<T extends FileStorageAdapter> {
    private _adapter;
    private _configuration;
    constructor(adapter: new (configuration: Record<string, any>) => T, configuration: Record<string, any>);
    upload(file: string, path: string): Promise<string>;
    upload(file: string, path: string, name: string): Promise<string>;
}
export = FileStorage;
