"use strict";
const branca = require("branca");
class Token {
    static instance() {
        if (typeof Token._instance === 'undefined') {
            Token._instance = branca(Token._key);
        }
        return Token._instance;
    }
    static key(key) {
        Token._key = key;
    }
    static read(token, ttl) {
        let content = null;
        if (typeof ttl !== 'number') {
            content = Token.instance().decode(token);
        }
        else {
            content = Token.instance().decode(token, ttl);
        }
        const payload = JSON.parse(content);
        return { token, payload };
    }
    static create(payload, ttl) {
        if (typeof ttl === 'number') {
            return {
                token: Token.instance().encode(JSON.stringify(payload), ttl),
                payload,
            };
        }
        return {
            token: Token.instance().encode(JSON.stringify(payload)),
            payload,
        };
    }
    static payload(token, payload) {
        if (typeof payload !== 'undefined') {
            const previousPayload = Token.read(token).payload;
            return Token.create({
                ...previousPayload,
                ...payload
            });
        }
        return Token.read(token);
    }
}
module.exports = Token;
