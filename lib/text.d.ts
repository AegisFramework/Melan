declare class Text {
    static capitalize(text: string): string;
    static suffix(key: string, text: string): string;
    static prefix(key: string, text: string): string;
    static friendly(text: string): string;
}
export = Text;
