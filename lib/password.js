"use strict";
const bcrypt = require("bcrypt");
class Password {
    static async hash(password) {
        return bcrypt.hash(password, Password.rounds);
    }
    static async compare(password, hash) {
        return bcrypt.compare(password, hash);
    }
}
Password.rounds = 12;
module.exports = Password;
