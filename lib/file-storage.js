"use strict";
class FileStorage {
    constructor(adapter, configuration) {
        this._configuration = Object.assign({ root: '/' }, configuration);
        this._adapter = new adapter(this._configuration);
    }
    async upload(file, path, name) {
        if (typeof name === 'string') {
            return this._adapter.upload(file, path, name);
        }
        return this._adapter.upload(file, path);
    }
}
module.exports = FileStorage;
