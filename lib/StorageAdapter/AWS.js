"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const s3_1 = __importDefault(require("aws-sdk/clients/s3"));
const file_storage_adapter_1 = __importDefault(require("./../file-storage-adapter"));
const util_1 = __importDefault(require("./../util"));
class AWS extends file_storage_adapter_1.default {
    constructor(configuration) {
        super(configuration);
        this._allowedMimeTypes = [];
        this._endpoint = '';
        this._region = '';
        this._bucket = '';
        this._root = '';
        const { apiVersion, region, endpoint, credentials, allow, bucket, root } = Object.assign({
            region: '',
            endpoint: '',
            allow: [],
            apiVersion: 'latest',
            bucket: '',
            root: '',
        }, configuration);
        const config = { apiVersion };
        if (typeof region === 'string' && region !== '') {
            config.region = region;
            this._region = region;
        }
        if (typeof endpoint === 'string' && endpoint !== '') {
            config.endpoint = endpoint;
            this._endpoint = endpoint;
        }
        if (typeof apiVersion === 'string' && apiVersion !== 'latest') {
            config.apiVersion = apiVersion;
        }
        else {
            config.apiVersion = 'latest';
        }
        if (allow instanceof Array) {
            this._allowedMimeTypes = allow;
        }
        else {
            this._allowedMimeTypes = [];
        }
        if (typeof credentials === 'object' && credentials !== null) {
            config.credentials = credentials;
        }
        if (typeof root === 'string' && root !== '') {
            this._root = root;
        }
        if (typeof bucket === 'string' && bucket !== '') {
            this._bucket = bucket;
            config.params = { Bucket: bucket };
        }
        this._instance = new s3_1.default(config);
    }
    instance() {
        return this._instance;
    }
    async upload(file, path, name) {
        return new Promise((resolve, reject) => {
            if (file.length === 0) {
                throw new Error('No data was provided.');
            }
            const instance = this.instance();
            const indicator = file.substr(0, 20);
            let fileExtension = '';
            let mimeType = '';
            for (const mime of this._allowedMimeTypes) {
                if (indicator.indexOf(mime) > 0) {
                    fileExtension = mime.split('/')[1];
                    mimeType = mime;
                    break;
                }
            }
            if (fileExtension === '') {
                throw new Error('UNSUPPORTED_FORMAT');
            }
            const base64Data = file.replace(`data:${mimeType};base64,`, '');
            const decodedFile = Buffer.from(base64Data, 'base64');
            if (typeof name !== 'string') {
                name = util_1.default.uuid();
            }
            instance.upload({
                Bucket: this._bucket,
                Body: decodedFile,
                Key: `${this._root}${path}${name}.${fileExtension}`,
                ContentEncoding: 'base64',
                ContentType: mimeType,
            }, (error, data) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data.Location);
                }
            });
        });
    }
}
module.exports = AWS;
