import S3 from 'aws-sdk/clients/s3';
import FileStorageAdapter from './../file-storage-adapter';
declare class AWS extends FileStorageAdapter {
    private _allowedMimeTypes;
    private _endpoint;
    private _region;
    private _bucket;
    private _root;
    private _instance;
    constructor(configuration: Record<string, any>);
    instance(): S3;
    upload(file: string, path: string): Promise<string>;
    upload(file: string, path: string, name: string): Promise<string>;
}
export = AWS;
