declare class Request {
    private _method;
    private _domain;
    private _resource;
    private _headers;
    private _queryParameters;
    private _pathParameters;
    private _data;
    constructor(method: string, domain: string, resource: string, headers: Record<string, string>, data: Record<string, string | number | object | null>, queryParameters: Record<string, string | number>, pathParameters: Record<string, string | number>);
    body(keys?: string[] | null, allowEmpty?: boolean, allowNull?: boolean): Record<string, string | number | object | null>;
    get method(): string;
    get domain(): string;
    get resource(): string;
    get path(): Record<string, string | number>;
    get query(): Record<string, string | number>;
    headers(): Record<string, string>;
    header(key: string): string;
    mobile(platform?: string): boolean;
    desktop(platform?: string): boolean;
    secure(): boolean;
}
export = Request;
