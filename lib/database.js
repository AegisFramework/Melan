"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const fs = require("fs");
const sequelize_1 = require("sequelize");
const secrets_1 = __importDefault(require("./secrets"));
class Database {
    static setup(engine) {
        this._engine = engine;
    }
    static async preload(models = {}) {
        const instance = await this.instance();
        for (const key in models) {
            const model = require(models[key])(instance.sequelize, sequelize_1.DataTypes);
            instance.models[key] = model;
        }
        return instance;
    }
    static async instance() {
        if (typeof this._instance === 'undefined') {
            this._instance = {
                models: {},
                sequelize: null,
                DataTypes: null,
                Sequelize: null
            };
            const [database, user, password, host] = await secrets_1.default.decrypt('DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_HOST');
            if (typeof database !== 'string' || typeof user !== 'string' || typeof password !== 'string' || typeof host !== 'string') {
                throw new Error('Invalid database credentials.');
            }
            let sequelize;
            if (process.env.ENVIRONMENT === 'production' || process.env.ENVIRONMENT === 'staging' || process.env.ENVIRONMENT === 'development') {
                const root = fs.readFileSync(`${__dirname}/../assets/aws-rds.pem`);
                sequelize = new sequelize_1.Sequelize(database, user, password, {
                    host,
                    dialect: this._engine,
                    ssl: true,
                    dialectOptions: {
                        ssl: {
                            rejectUnauthorized: true,
                            ca: [root],
                        },
                    },
                });
            }
            else {
                sequelize = new sequelize_1.Sequelize(database, user, password, {
                    host,
                    dialect: this._engine
                });
            }
            this._instance.sequelize = sequelize;
            this._instance.Sequelize = sequelize_1.Sequelize;
            this._instance.DataTypes = sequelize_1.DataTypes;
            return this._instance;
        }
        else {
            return this._instance;
        }
    }
    static async models(models) {
        const instance = await this.instance();
        if (typeof models === 'object' && models !== null) {
            for (const key in models) {
                if (!(key in instance.models)) {
                    const model = require(models[key])(instance.sequelize, sequelize_1.DataTypes);
                    instance.models[key] = model;
                }
            }
        }
        return this._instance.models;
    }
    static async model(name, path) {
        const instance = await this.instance();
        if (!(name in instance.models)) {
            const model = require(path)(instance.sequelize, sequelize_1.DataTypes);
            instance.models[model.name] = model;
        }
        return this._instance.models[name];
    }
}
Database._engine = 'mariadb';
module.exports = Database;
