"use strict";
const uuid_1 = require("uuid");
class Util {
    static callAsync(callable, context, ...args) {
        try {
            const result = callable.apply(context, args);
            if (result instanceof Promise) {
                return result;
            }
            else {
                return Promise.resolve(result);
            }
        }
        catch (e) {
            return Promise.reject(e);
        }
    }
    static uuid() {
        return uuid_1.v4();
    }
}
module.exports = Util;
