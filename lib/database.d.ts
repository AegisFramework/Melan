import { Model } from 'sequelize';
declare type DatabaseEngine = 'mysql' | 'postgres' | 'sqlite' | 'mariadb' | 'mssql' | undefined;
interface DatabaseInterface {
    models: Record<string, Model>;
    sequelize: any;
    DataTypes: any;
    Sequelize: any;
}
declare class Database {
    private static _engine;
    private static _instance;
    static setup(engine: DatabaseEngine): void;
    static preload(models?: Record<string, string>): Promise<DatabaseInterface>;
    static instance(): Promise<DatabaseInterface>;
    static models(models?: Record<string, string>): Promise<Record<string, Model>>;
    static model(name: string, path: string): Promise<Model>;
}
export = Database;
