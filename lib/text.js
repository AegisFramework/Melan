"use strict";
class Text {
    static capitalize(text) {
        return text.replace(/\w\S*/g, (txt) => {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
    static suffix(key, text) {
        let suffix = '';
        let position = text.indexOf(key);
        if (position !== -1) {
            position += key.length;
            suffix = text.substr(position, text.length - position);
        }
        return suffix;
    }
    static prefix(key, text) {
        let prefix = '';
        const position = text.indexOf(key);
        if (position != -1) {
            prefix = text.substr(0, position);
        }
        return prefix;
    }
    static friendly(text) {
        const regex = [
            /[áàâãªä]/,
            /[ÁÀÂÃÄ]/,
            /[ÍÌÎÏ]/,
            /[íìîï]/,
            /[éèêë]/,
            /[ÉÈÊË]/,
            /[óòôõºö]/,
            /[ÓÒÔÕÖ]/,
            /[úùûü]/,
            /[ÚÙÛÜ]/,
            /ç/,
            /Ç/,
            /ñ/,
            /Ñ/,
            /_/,
            /[’‘‹›<>']/,
            /[“”«»„"]/,
            /[(){}[\]]/,
            /[?¿!¡#$%&^*´`~/°|]/,
            /[,.:;]/,
            / /
        ];
        const replacements = [
            'a',
            'A',
            'I',
            'i',
            'e',
            'E',
            'o',
            'O',
            'u',
            'U',
            'c',
            'C',
            'n',
            'N',
            '-',
            '',
            '',
            '',
            '',
            '',
            '-'
        ];
        for (const index in regex) {
            text = text.replace(new RegExp(regex[index], 'g'), replacements[index]);
        }
        return text;
    }
}
module.exports = Text;
