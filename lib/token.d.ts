interface Branca {
    encode: Function;
    decode: Function;
}
interface TokenInstance {
    token: string;
    payload: Record<string, any>;
}
declare class Token {
    private static _key;
    private static _instance;
    static instance(): Branca;
    static key(key: string): void;
    static read(token: string): TokenInstance;
    static read(token: string, ttl: number): TokenInstance;
    static create(payload: Record<string, any>): TokenInstance;
    static create(payload: Record<string, any>, ttl: number): TokenInstance;
    static payload(token: string, payload?: Record<string, any>): TokenInstance;
}
export = Token;
