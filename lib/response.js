"use strict";
const Status = {
    100: 'CONTINUE',
    200: 'OK',
    201: 'CREATED',
    202: 'ACCEPTED',
    204: 'NO_CONTENT',
    205: 'RESET_CONTENT',
    301: 'MOVED_PERMANENTLY',
    302: 'FOUND',
    400: 'BAD_REQUEST',
    401: 'UNAUTHORIZED',
    402: 'PAYMENT_REQUIRED',
    403: 'FORBIDDEN',
    404: 'NOT_FOUND',
    405: 'METHOD_NOT_ALLOWED',
    406: 'NOT_ACCEPTABLE',
    409: 'CONFLICT',
    500: 'SERVER_ERROR',
    503: 'SERVICE_UNAVAILABLE',
};
class Response {
    constructor(context, callback) {
        this._status = 'OK';
        this._code = 200;
        this._type = '';
        this._context = context;
        this._body = {};
        this._headers = {};
        this._callback = callback;
    }
    status(status) {
        this._status = status;
    }
    code(code) {
        this._code = code;
    }
    body(data) {
        if (data instanceof Array || typeof data === 'string' || typeof data === 'number') {
            this._body = data;
        }
        else if (typeof data === 'object') {
            this._body = { ...data, ...this._body };
        }
    }
    headers(headers) {
        if (typeof headers !== 'undefined') {
            this._headers = Object.assign(this._headers, headers);
        }
        return this._headers;
    }
    header(key, value) {
        this._headers[key] = value;
    }
    send() {
        const data = this.toJSON();
        return {
            statusCode: this._code,
            headers: this._headers,
            body: typeof data === 'object' ? JSON.stringify(data) : data,
        };
    }
    error(error) {
        this.code(error.code);
        this.status(Status[error.code]);
        this._type = error.type || '';
        this.body({
            message: error.message || '',
            type: this._type
        });
        const data = this.toJSON();
        return {
            code: this._code,
            status: this._status,
            type: this._type || 'SERVER_ERROR',
            ...data,
        };
    }
    allow(origin) {
        if (origin === '*') {
            this.header('Access-Control-Allow-Origin', origin);
        }
        else {
            this.headers({
                'Access-Control-Allow-Origin': origin,
                'Vary': 'Origin',
            });
        }
    }
    toJSON() {
        return this._body;
    }
}
module.exports = Response;
