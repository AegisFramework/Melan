import KMS from 'aws-sdk/clients/kms';
interface SecretsInstance {
    env: string;
    kms?: KMS;
    secrets: Record<string, {
        [key: string]: {
            ciphertext: string;
            arn: string;
        };
    } | string | undefined>;
    plain: Record<string, {
        cipher: string;
        text: string;
    }>;
}
declare class Secrets {
    private static _instance;
    private static _file;
    static instance(): SecretsInstance;
    static file(path: string): void;
    static environment(): string;
    static decrypt(...keys: string[]): Promise<Array<string | {
        [key: string]: {
            ciphertext: string;
            arn: string;
        };
    }>>;
    static raw(...keys: string[]): Promise<Array<string | {
        [key: string]: {
            ciphertext: string;
            arn: string;
        };
    }>>;
}
export = Secrets;
