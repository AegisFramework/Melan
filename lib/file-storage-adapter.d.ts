declare class FileStorageAdapter {
    constructor(configuration: Record<string, any>);
    upload(file: string, path: string): Promise<string>;
    upload(file: string, path: string, name: string): Promise<string>;
}
export = FileStorageAdapter;
