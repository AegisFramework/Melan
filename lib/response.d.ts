interface ErrorInterface {
    code: number;
    type?: string;
    message?: string;
}
declare class Response {
    private _status;
    private _code;
    private _type;
    private _body;
    private _headers;
    private _callback;
    private _context;
    constructor(context: any, callback: Function);
    status(status: string): void;
    code(code: number): void;
    body(data: Record<string, string | number | null | object> | Array<any>): void;
    headers(headers?: Record<string, string | number>): Record<string, string | number>;
    header(key: string, value: string): void;
    send(): any;
    error(error: ErrorInterface): any;
    allow(origin: string): void;
    toJSON(): Record<string, string | number | null | object> | Array<any>;
}
export = Response;
