"use strict";
class Request {
    constructor(method, domain, resource, headers, data, queryParameters, pathParameters) {
        this._method = method;
        this._domain = domain;
        this._resource = resource;
        this._headers = headers;
        if (method === 'GET') {
            this._data = queryParameters;
        }
        else {
            if (typeof data === 'string') {
                this._data = JSON.parse(data);
            }
            else {
                this._data = data;
            }
        }
        this._queryParameters = queryParameters;
        this._pathParameters = pathParameters;
    }
    body(keys = null, allowEmpty = false, allowNull = false) {
        if (keys !== null) {
            const object = {};
            for (const key of keys) {
                if (typeof this._data[key] !== 'undefined') {
                    const value = this._data[key];
                    if (!allowEmpty) {
                        if (typeof value === 'string') {
                            if (value.trim() === '') {
                                throw new Error(`Requested key ${key} was empty.`);
                            }
                        }
                    }
                    if (!allowNull && value === null) {
                        throw new Error(`Requested key ${key} can not be null.`);
                    }
                    object[key] = value;
                }
                else {
                    if (!allowNull) {
                        throw new Error(`Requested key ${key} was not present in the request data.`);
                    }
                }
            }
        }
        return this._data;
    }
    get method() {
        return this._method;
    }
    get domain() {
        return this._domain;
    }
    get resource() {
        return this._resource;
    }
    get path() {
        return this._pathParameters;
    }
    get query() {
        return this._queryParameters;
    }
    headers() {
        return this._headers;
    }
    header(key) {
        return this._headers[key];
    }
    mobile(platform = 'Any') {
        const userAgent = this.header('User-Agent');
        let match = false;
        switch (platform) {
            case 'Android':
                match = /Android/i.test(userAgent);
                break;
            case 'iOS':
                match = /iPhone|iPad|iPod/i.test(userAgent);
                break;
            case 'Windows':
                match = /Windows Phone|IEMobile|WPDesktop/i.test(userAgent);
                break;
            case 'BlackBerry':
                match = /BlackBerry|BB10/i.test(userAgent);
                break;
            case 'Any':
            default:
                match = /Android|iPhone|iPad|iPod|Windows Phone|IEMobile|WPDesktop|BlackBerry|BB10/i.test(userAgent);
                break;
        }
        return match;
    }
    desktop(platform = 'Any') {
        const userAgent = this.header('User-Agent');
        let match = false;
        switch (platform) {
            case 'Windows':
                match = userAgent.includes('Win');
                break;
            case 'macOS':
                match = userAgent.includes('Mac');
                break;
            case 'Linux':
                match = userAgent.includes('Linux');
                break;
            case 'FreeBSD':
                match = userAgent.includes('FreeBSD');
                break;
            case 'webOS':
                match = userAgent.includes('WebTV');
                break;
            case 'Any':
            default:
                match = userAgent.includes('Win')
                    || userAgent.includes('Mac')
                    || userAgent.includes('Linux')
                    || userAgent.includes('FreeBSD')
                    || userAgent.includes('WebTV');
                break;
        }
        return match;
    }
    secure() {
        return this.header('X-Forwarded-Proto') === 'https';
    }
}
module.exports = Request;
