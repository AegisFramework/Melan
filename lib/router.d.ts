import Response from './response';
import Request from './request';
interface APIGatewayEvent {
    event?: APIGatewayEvent;
    resource: string;
    path?: string;
    httpMethod?: string;
    headers: Record<string, any>;
    multiValueHeaders?: Record<string, any[]>;
    queryStringParameters: Record<string, any>;
    multiValueQueryStringParameters?: Record<string, any[]>;
    pathParameters: Record<string, any>;
    stageVariables: Record<string, any>;
    requestContext: Record<string, any>;
    body: Record<string, any>;
    isBase64Encoded: boolean;
    http?: {
        method: string;
        path: string;
    };
    source?: string;
}
interface PaginatedResponse {
    previous: string;
    current: string;
    next: string;
    count: number;
    results: any;
}
interface PaginationOptions {
    results: any;
    page: number;
    size: number;
    count: number;
    params: Record<string, string | number>;
}
declare class Router {
    private _event;
    private _context;
    private _callback;
    private _response;
    private _request;
    private _stage;
    private _origins;
    static serialize(params: Record<string, string | number>): string;
    paginate(options: PaginationOptions): PaginatedResponse;
    static mime(extension: string): string;
    constructor(event: APIGatewayEvent, context: Record<string, any>, callback: Function);
    get request(): Request;
    get response(): Response;
    get context(): any;
    get event(): APIGatewayEvent;
    get stage(): any;
    allow(...origins: string[]): void;
    authorize(ttl: number, sessionKey?: string, secretKey?: string): Promise<{
        token: string;
        payload: Record<string, any>;
    }>;
    serve(callback: Function): Promise<void>;
}
export = Router;
