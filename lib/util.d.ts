declare class Util {
    static callAsync(callable: Function, context: any, ...args: any[]): any;
    static uuid(): string;
}
export = Util;
